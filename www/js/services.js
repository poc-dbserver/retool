

angular.module('app.services', [])
.factory(("ionPlatform"), function( $q ){
    var ready = $q.defer();

    ionic.Platform.ready(function( device ){
        ready.resolve( device );
    });

    return {
        ready: ready.promise
    }
})

.factory('postTokenFactory', function($http) {
    return {
        enviaToken : function(token,deviceId){
            
            console.log("Token"+token);
            console.log("deviceID"+deviceId);
            var data = {
                registrationId: token,
                deviceId:deviceId
            }

        //var url = "http://10.96.127.144:3000/api/devices";
        var url = "https://retool-api-adrianlemess.c9users.io/api/devices";
        var req = {
         method: 'POST',
         url: url,				   
         data: data
     }

     $http(req).then(function sucessCallback(response){				
        return response.data;
    }, function errorCallback(response){

        return response;
    });		
     console.log(data);
 }   
}

})
.factory('IndicatorsService', function($http) {
    return {
      getListIndicators: function(){
            //return $http.get("http://10.96.127.144:3000/api/indicators").then(function(response){
             return $http.get("https://retool-api-adrianlemess.c9users.io/api/indicators").then(function(response){
              var items = response.data; 
              return items;
          });
         }
     }
 })


.service('relativeTimeService', function($rootScope){  

        this.getRelativeTime = function(responses){
            var items = [];
            var count = -1;
            for (obj of responses){
                count=count+1;
                var dia = new Date(obj.data).getDate();
                var atual = new Date().getDate();
                var data = new Date(obj.data);
                var hora = new Date(obj.data).getTime() - new Date().getTime();
                hora = ((hora/3600000)*60)*(-1);
                hora = new Date(hora).getTime();
                if (dia == atual){
                    if(hora > 60){
                        hora = hora/60;
                        hora = parseInt(hora);
                        obj.timeRelative = hora.toFixed(0);
                        obj.timeRelative = "Há "+obj.timeRelative + "h"; 
                    }else {
                        obj.timeRelative = hora.toString();
                        obj.timeRelative = "Há "+obj.timeRelative + "m";
                    }
                }else {
                    obj.timeRelative = data.getDate() + "/" + data.getMonth() + "/" + data.getFullYear() + " - "+ data.getHours() +"h:"+data.getMinutes()+"m";
                }
                obj.count = count;
                items.push(obj);
            }
            console.log(items);
            $rootScope.items = items;
         
        }
    });

