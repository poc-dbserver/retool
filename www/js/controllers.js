angular.module('app.controllers', ['angular.filter'])
.controller('timelineCtrl', function($scope, IndicatorsService,postTokenFactory,relativeTimeService, $ionicLoading){
        $scope.countBadge = 0;
        $scope.count = 0;
    
    $scope.convertDay = function(dateToConvert){
        var newDate = new Date(dateToConvert);
        return newDate.getDate();
    }
   
    $scope.convertDate = function(dateToConvert){
        var dataAtual = new Date();
        var dataNova = new Date(dateToConvert);
        if (dataNova.getDate() == dataAtual.getDate()){
        //console.log("true")
            return true;

        }else {
        //console.log("false")
            return false;
        }
    };
    
    $scope.class = "min-lines corpo";
	$scope.changeClass = function(id){
        
		if(document.getElementById('corpo'+id).className == "min-lines corpo"){
			document.getElementById('corpo'+id).className="corpo";
            document.getElementById('button'+id).innerHTML = "<i class='button-icon-left ion-arrow-up-b'></i> Menos Detalhes <i class='button-icon-right ion-arrow-up-b' ></i>";	 
		}else{
			document.getElementById('corpo'+id).className="min-lines corpo";
            document.getElementById('button'+id).innerHTML = "<i class='button-icon-left ion-arrow-down-b'></i> Mais Detalhes <i class='button-icon-right ion-arrow-down-b' ></i>";	                	
        }
	};
    
    $scope.uncolapseCard = function(cardId) {
              if (document.getElementById('corpo'+cardId).className=="corpo")
              document.getElementById('corpo'+cardId).className="min-lines corpo";
                document.getElementById('button'+cardId).innerHTML = "<i class='button-icon-left ion-arrow-down-b'></i> Mais Detalhes <i class='button-icon-right ion-arrow-down-b' ></i>";	       
        }
    
$scope.refresh = function(){
    $scope.countBadge = 0;
    return IndicatorsService.getListIndicators().then(function(responses){
        var push = PushNotification.init({
            android: {
                senderID: "67893829600"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        });
        
        var deviceInformation = ionic.Platform.device();
        var initRegister = function(){
            push.on('registration', function(data) {
                //deviceInformation.uuid
                console.log("registration: "+data.registrationId);
                postTokenFactory.enviaToken(data.registrationId,deviceInformation.uuid);
                //console.log("My Device token:"+token.token,token.token);
                //push.saveToken(data.registrationId);  // persist the token in the Ionic Platform
            })
        }
        initRegister();
        
        push.on('notification', function(data) {
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData
            //alert(data.message);
            $scope.$apply(function() {
                $scope.countBadge++;
            });

        });

        push.on('error', function(e) {
            // e.message
            alert(e.message);
        });

        var variavel = relativeTimeService.getRelativeTime(responses);
        
    })
    };
  
    $scope.refresh();
    $scope.doRefresh = function() {
         $scope.$apply(function() {
        $scope.countBadge = 0;
         });
        setTimeout(function(){
            $scope.refresh().finally(function(){
                    $scope.countBadge = 0;
                $scope.$broadcast('scroll.refreshComplete');
                $ionicLoading.show({ template: 'Timeline Atualizada!', noBackdrop: false, duration: 1000 });
                
            });               
        },1200)
    }
})



